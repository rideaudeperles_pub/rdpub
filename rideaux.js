let couleurs = [
  "blue",
  "purple",
  "pink",
  "brown",
  "orange",
  "green",
  "blue",
  "pink",
  "red",
];
const SCREEN_W = window.innerWidth;
const AMP = 1.3;
function random(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}
const rideau = document.createElement("div");
rideau.classList.add("rideau");
document.querySelector("body").appendChild(rideau);
for (let cols = 0; cols < 80; cols++) {
  const fil = document.createElement("div");
  fil.classList.add("fil");
  for (let i = 0; i < 200; i++) {
    const perle = document.createElement("span");
    perle.classList.add("perle");
    perle.style.height = AMP * Math.random() * 10 + 4 + "px";
    perle.style.width = AMP * Math.random() * 0.5 + 4 + "px";
    perle.style.transform = `rotate(${Math.random() * 20 - 10}deg)`;
    /*perle.style.borderRadius = `${Math.random() * 100}% ${
      Math.random() * 100
    }% ${Math.random() * 100}% ${Math.random() * 100}% `;*/
    perle.style.background = `linear-gradient(${random(couleurs)}, ${random(
      couleurs
    )})`;
    perle.style.borderRadius = "50%";
    fil.appendChild(perle);
  }
  document.querySelector(".rideau").appendChild(fil);
  console.log(fil.offsetHeight, window.innerHeight);
}
