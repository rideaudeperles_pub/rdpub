const CSVToJSON = (csv) => {
  const lines = csv.replaceAll("\t", "\n").split("\n");
  const keys = lines[0].split(",");
  const regex = /(?!\B"[^"]*),(?![^"]*"\B)/g;
  //  console.log(typeof regex, regex);
  return lines.slice(1).map((line) => {
    return line.split(regex).reduce((acc, cur, i) => {
      const toAdd = {};
      keys[i] = keys[i].toLowerCase().replaceAll(" ", "-");

      toAdd[keys[i]] = cur.trim().replaceAll('"', "");
      return { ...acc, ...toAdd };
    }, {});
  });
};

const clearEmptyRow = (csv) => {
  let res = [];
  for (let i = 0; i < csv.length; i++) {
    if (csv[i].titre && csv[i].titre !== "") {
      res = [...res, csv[i]];
    }
  }
  return res;
};

const getCollection = (data, collections) => {
  data.forEach((item, index) => {
    if (
      item.data.collections === undefined ||
      item.data.collections.length === 0
    ) {
    } else {
      for (const collection of item.data.collections) {
        let found = collections.find((e) => e.data.key === collection);
        console.log(item.data.title, "-", found.data.name);
      }
    }
  });
};
const getLivresFromCollection = (collection, zoteroItems, zoteroCol, ctn) => {
  let span = document.createElement("span");

  let res = [];

  const collectionLinked = zoteroCol.find((e) => collection === e.data.name);
  if (collectionLinked) {
    for (const livre of zoteroItems) {
      if (livre.data.collections == collectionLinked.key) {
        res = [...res, livre];
        span.innerHTML += livre;
      }
    }
    ctn.appendChild(span);
    console.log(collection, res);
  }

  if (collectionLinked !== undefined) {
    zoteroItems.find((e) => collectionLinked.key === e.data.collections);
  }
  return res;
};

const parse = (string) => {
  const res = Papa.parse(string, {
    header: true,
    transformHeader: function (e) {
      return sanitizeString(e.trim().toCamelCase());
    },
  });
  return res.data;
};
const sanitizeString = (str) => {
  str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, "");
  return str.trim();
};
String.prototype.toCamelCase = function () {
  return this.replaceAll("-", "").replace(
    /^([A-Z])|\s(\w)/g,
    function (match, p1, p2, offset) {
      if (p2) return p2.toUpperCase();
      return p1.toLowerCase();
    }
  );
};
