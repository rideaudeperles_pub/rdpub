


const body = document.querySelector("body");
const genParutions = (data) => {
  const container = document.createElement("section");
  // genTitre("Extraits", container);
  const ul = document.createElement("ul");
  container.appendChild(ul);

  for (const e of data) {
        
    
        const titre = document.createElement("div");
        titre.classList.add("box");
        titre.innerHTML = `<span class="titre">${e.data.title}</span>`;
        ul.appendChild(titre);

        const item = document.createElement("div");
        item.classList.add("item");
        item.innerHTML = `<span class="titre">${e.data.itemType}</span>`;
        ul.appendChild(item);


        // const date = document.createElement("div");
        // date.classList.add("date");
        // date.innerHTML = `<span>${e.data.date}</span>`;
        // ul.appendChild(date);

        const resume = document.createElement("li");
        resume.classList.add("resume");
        resume.innerHTML = `<span> ${e.data.abstractNote} </span>`;
        ul.appendChild(resume);
  }

//ne pas afficher extrait si pas d'extrait 

// 1 extrait aléatoire : 
  // maxwidth : 100%, maxheight: 100%, > si beaucoup de texte petit, mais si pas beaucoup, plus gros



  body.appendChild(container);
};


const genInfos = (data) => {
    const container = document.createElement("section");
    genTitre("informations", container);
    const description = document.createElement("p");
    description.innerHTML = data[2].contenu;
    container.appendChild(description);
  };
  const auteurices = (entree) => {
    let res = "";
    for (const a of entree.data.creators) {
      res += a.firstName;
      res += " ";
      res += a.lastName1
      res += ", ";
    }
    return res;
  };
  
  const genTitre = (string, section) => {
    const titre = document.createElement("h1");
    titre.innerHTML = string;
    section.appendChild(titre);
  };
  
  const upperCaseFirstLetter = (string) =>
    `${string.slice(0, 1).toUpperCase()}${string.slice(1)}`;
  