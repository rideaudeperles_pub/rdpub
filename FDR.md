# Appeler les données

- Formater les donnés
- Définir des flux

# Mise en page

- Feuilles de styles
  - Feuille générale
  - Feuille spécifique

# Gestion de la timeline

- À faire lorsque nous auront suffisament de contenu

---

# Flux

## Infos

- Partie info générales du calc

## Derniers ajouts

- zotero formaté
  - Titre + Nom Prénom (du livre)

## Agenda

- agenda calc
- zotero jointure

## Extraits

- zotero formaté
  - Résumés qui sont récupérés, espace de vitrine mise en avant
