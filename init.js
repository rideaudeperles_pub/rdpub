const couleurs = [
  "greenyellow",
  "olive",
  "lightcyan",
  "green",
  "lightgray",
  "purple",
  "pink",
  "white",
  "lightblue",
];
const emojis = ["(ɔ◔‿◔)ɔ ♥", "(╥︣﹏᷅╥)", "≧◠‿◠≦✌", "≧◠‿●‿◠≦", "(っ＾▿＾)"];
Array.prototype.sample = function () {
  return this[Math.floor(Math.random() * this.length)];
};

const direction = ["up", "down"]; //, "left", "right"];

const genContent = () => {
  let res = "";
  for (let i = 0; i < 1 + Math.random() * 65; i++) {
    Math.random() < 0.4 ? (res += "<br>") : (res += "");
    res += emojis.sample() + " ";
  }
  return res;
};
const marquees = () => {
  for (let i = 0; i < 1 + Math.random() * 3; i++) {
    let e = document.createElement("section");
    let content = genContent();
    //Math.random() < 0.5 ? (e.style.breakBefore = "always") : "";
    e.innerHTML = ` <marquee behavior="alternate" direction="${direction.sample()}" scrollamount="${
      Math.random() * 10 + 2
    }" scrolldelay="${60}" >${content}</marquee>`;
    e.setAttribute("id", i);
    //  e.style.background = `linear-gradient(${couleurs.sample()}, ${couleurs.sample()})`;
    e.style.background = `${couleurs.sample()}`;
    e.style.border = `1px solid lightgray`;
    e.style.flexGrow = Math.round(Math.random() * 9 + 1);
    document.querySelector("body").appendChild(e);
  }
};
