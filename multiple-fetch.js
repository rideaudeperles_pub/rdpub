const urls = [
  "https://lite.framacalc.org/j7hrie4qel-9y14.csv",
  "https://api.zotero.org/groups/2622649/items?&v=3&key=Awyq7k6Ff7tkq97aG6IyKm1S&format=json&limit=100",
  "https://api.zotero.org/groups/2622649/collections?v=3&key=Awyq7k6Ff7tkq97aG6IyKm1S&format=json",
];
// pour chaque urls, attendre de les recevoir et lire la réponse au format text
Promise.all(urls.map((url) => fetch(url).then((value) => value.text())))
  // on les met dans un tableau
  .then(([framacalc, zoteroItems, zoteroCol]) => {
    framacalc = parse(framacalc);
    //clickListener();
    zoteroItems = JSON.parse(zoteroItems);
    zoteroCol = JSON.parse(zoteroCol);
    switch (window.location.pathname) {
      case "/":
      case "/index.html":
        genParutions(zoteroItems);
        getCollection(zoteroItems, zoteroCol);
        genInfos(framacalc);
        break;
      case "/infos.html":
        genPageInfos(framacalc);
        break;

      case "/agenda.html":
        genPageAgenda(framacalc, zoteroItems, zoteroCol);
        break;
      default:
        console.log("default");
        break;
    }
  })
  .catch((err) => {
    console.log(err);
  });
