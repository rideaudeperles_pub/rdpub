const body = document.querySelector("body");
const genParutions = (data) => {
  const container = document.createElement("section");
  genTitre("Nouveautés", container);
  const ul = document.createElement("ul");
  container.appendChild(ul);
  for (const e of data) {
    const elem = document.createElement("li");
    elem.innerHTML = `<marquee class="titre">${
      e.data.title
    }</marquee><marquee class="auteurices">${auteurices(e)}</marquee> `;
    ul.appendChild(elem);
  }

  body.appendChild(container);
};
const genInfos = (data) => {
  const container = document.createElement("section");
  genTitre("informations", container);
  const description = document.createElement("p");
  description.innerHTML = data[2].contenu;
  container.appendChild(description);
  body.appendChild(container);
};
const genPageInfos = (data) => {
  const container = document.createElement("section");
  const div1 = document.createElement("div");
  div1.setAttribute('id', 'insta');
  const insta = document.createElement("marquee");
  insta.setAttribute('direction', 'up');
  insta.innerHTML = data[1].contenu;
  div1.appendChild(insta);
  const div2 = document.createElement("div");
  div2.setAttribute('id', 'mail');
  const mail = document.createElement("marquee");
  mail.innerHTML = data[0].contenu;
  div2.appendChild(mail);
  const div3 = document.createElement("div");
  div3.setAttribute('id', 'description');
  const description = document.createElement("p");
  div3.appendChild(description);
  description.innerHTML = data[2].contenu;
  description.setAttribute("id", "description");

  container.appendChild(div1);
  container.appendChild(div2);
  container.appendChild(div3);

  body.appendChild(container);
};
const genPageAgenda = (framacalc, zoteroItems, zoteroCol) => {
  const container = document.createElement("section");
  const agenda = document.createElement("div");
  agenda.classList.add("agenda");
  for (const entree of framacalc) {
    if (entree.date !== "") {
      const ligneAgenda = document.createElement("section");
      const date = document.createElement("span");
      const nomDeLevenement = document.createElement("span");
      const agendaCollection = document.createElement("span");
      const agendaLieu = document.createElement("span");
      const resCtn = document.createElement("div");

      getLivresFromCollection(
        entree.collection,
        zoteroItems,
        zoteroCol,
        resCtn
      );

      date.innerHTML = entree.date;
      agendaCollection.innerHTML = entree.collection;
      nomDeLevenement.innerHTML = entree.nomDeLevenement;
      agendaLieu.innerHTML = entree.lieu;
      [date, nomDeLevenement, agendaCollection, agendaLieu, resCtn].forEach(
        (e) => {
          ligneAgenda.appendChild(e);
        }
      );

      agenda.appendChild(ligneAgenda);
    }
  }
  container.appendChild(agenda);
  body.appendChild(container);
};

const auteurices = (entree) => {
  let res = "";

  if (entree.data.creators) {
    for (const a of entree.data.creators) {
      if (a) {
        res += a.firstName;
        res += " ";
        res += a.lastName;
        res += ", ";
      }
    }
  }
  return res;
};

const genTitre = (string, section) => {
  const titre = document.createElement("h1");
  titre.innerHTML = string;
  section.appendChild(titre);
};

const upperCaseFirstLetter = (string) =>
  `${string.slice(0, 1).toUpperCase()}${string.slice(1)}`;
